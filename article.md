---
title: "Projekty na Hithitu a Startovači získaly prvních 100 milionů korun. Podívejte se, kdo uspěl"
perex: "Dva největší české klony Kickstarteru pomohly na svět bezmála tisícovce nápadů. Většinou jde o hudbu a knihy, technologickým novinkám se u nás příliš nedaří."
description: "Dva největší české klony Kickstarteru pomohly na svět bezmála tisícovce nápadů. Většinou jde o hudbu a knihy, technologickým novinkám se u nás příliš nedaří."
authors: ["Jan Boček"]
published: "21. listopadu 2016"
coverimg: https://interaktivni.rozhlas.cz/startovace-text/media/cover.jpg
coverimg_note: "Foto: <a href='http://tichomori.transtrabant.cz/'>Trabantem napříč kontinenty</a>"
url: "startovace-text"
socialimg: https://interaktivni.rozhlas.cz/startovace-text/media/socialimg_new.png
libraries: [d3, jquery, highcharts]
recommended:
  - link: https://interaktivni.rozhlas.cz/usvolby-explainer/
    title: Sedm momentů, které dovedly Ameriku k Trumpovi
    perex: Jak a proč se vyvíjela americká volební mapa?
    image: https://interaktivni.rozhlas.cz/usvolby-explainer/media/washington.jpg
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/loterie-kde-999-losu-vyhrava-vyzkousejte-si-v-interaktivni-hre-jak-je-to-doopravdy--1594792
    title: Loterie, kde 99,9 % losů vyhrává?
    perex: Vyzkoušejte si v interaktivní hře, jak je to doopravdy
    image: https://interaktivni.rozhlas.cz/hazard/media/2521b6ae119fecada5488c82f544af65/450x_.jpeg
  - link: http://www.rozhlas.cz/zpravy/technika/_zprava/stoleti-brouka-hitlerovo-lidove-auto-prezilo-valku-a-svet-si-ho-zamiloval--1584151
    title: Století brouka
    perex: Hitlerovo lidové auto přežilo válku a svět si ho zamiloval
    image: https://samizdat.cz/data/volkswagen-80/www/media/img_front.jpg
---

Americký web Kickstarter spustil v roce 2009 revoluci pro hudebníky, spisovatele, autory videoher a další tvůrce: na svém webu zprostředkuje kontakt mezi autory a drobnými zájemci o jejich díla. Díky tomu můžou peníze pro svůj nápad vybrat ještě předtím, než se do něj pustí. Tak vznikly například [brýle pro virtuální realitu Oculus Rift](https://www.kickstarter.com/projects/1523379957/oculus-rift-step-into-the-game), na jejichž vývoj přispěli zájemci 2,5 milionu dolarů, nebo vůbec nejúspěšnější kampaň na [hodinky Pebble](https://www.kickstarter.com/projects/597507018/pebble-time-awesome-smartwatch-no-compromises), které svým designérům přilákaly přes 20 milionů dolarů.

O tři roky později se v Česku objevilo několik podobných webů. Z necelé desítky kopií Kickstarteru časem vykrystalizovaly dva nejúspěšnější: Hithit, přes který autoři nápadů dosud získali 67 milionů korun, a Startovač s 45 miliony. Od jejich začátků v roce 2012 do dneška na nich uspěla necelá tisícovka nápadů. Podívejte se, které.

*Velikost kruhu říká, kolik peněz projekt získal, po najetí myši se zobrazí jeho popis. Šířka barevného tlačítka vpravo od grafu ukazuje, kolik peněz získaly jednotlivé kategorie. Kliknutím na tlačítko si zobrazíte zvolenou kategorii.*

<aside class="big chuchvalce">
  <div class="chuchvalce-hithit">
    <h3>Všechny úspěšné projekty z HitHitu</h3>
    <div class="scroll-wrapper">
      <iframe src="https://samizdat.cz/data/startovace/charts/chuchel-hithit.html"   frameborder=0></iframe>
    </div>
  </div>
  <div class="chuchvalce-startovac">
    <h3>Všechny úspěšné  projekty ze Startovače</h3>
    <div class="scroll-wrapper">
      <iframe  src="https://samizdat.cz/data/startovace/charts/chuchel-startovac.html" frameborder=0></iframe>
    </div>
  </div>
</aside>

*Seznam úspěšných i neúspěšných projektů si také můžete stáhnout v tabulce: [Hithit](https://samizdat.cz/data/startovace/charts/hithit-zdroj.zip) a [Startovač](https://samizdat.cz/data/startovace/charts/startovac-zdroj.csv).*

## Česko: spousta začínajících kapel, chybí technologické nápady

Nejúspěšnější jsou v Česku hudebníci. Jsou mezi nimi známé tváře: Mňága a Žďorp, Xavier Baumaxa, Lenka Dusilová, kapely Vltava nebo Už jsme doma. Vedle nich o peníze fanoušků usiluje spousta začínajících nebo méně známých kapel, které obvykle chtějí drobnější příspěvek. Na Hithitu hudebníci průměrně získají 95 tisíc korun, na Startovači 70 tisíc.

Silné zastoupení mezi úspěšnými nápady mají také autoři knih; jak píšící autoři, tak kreslíři nebo fotografové.

„V podstatě kopírujeme vývoj v zahraničí, jen s určitým zpožděním,“ vysvětluje Petr Tomek ze Startovače. „I ve světě byly první hudební a komunitní projekty, jenže to bylo na přelomu tisíciletí.“

Slabý podíl na Hithitu i Startovači mají naopak technologické projekty a videohry. Přitom na americkém Kickstarteru dokázaly právě ony [získat největší částky](https://www.kickstarter.com/help/stats). Úspěšné technologické projekty jako zmíněný Oculus Rift nebo Pebble Time totiž platformu obvykle využívají k předprodeji svých produktů, takže se často vybere mnohonásobně víc, než autoři původně vyžadují. Designéři hodinek Pebble vybrali požadovaný půlmilion dolarů během sedmnácti minut, nakonec dostali víc než čtyřicetkrát tolik.

„V české společnosti technické nápady rozhodně jsou, ale je to komunita, která se prostě crowdfunding ještě nenaučila,“ tvrdí Tomek.

„Souvisí to samozřejmě i s velikostí trhu,“ doplňuje jeho kolega Jakub Červinka. „Vývoj nové technologie je většinou značně finančně náročný. Autoři potřebují oslovit více trhů najednou.“

Řada českých technologických projektů s většími ambicemi proto míří na americký Kickstarter. Některé si postupně vyzkoušejí české i mezinárodní prostředí.

„Autoři [ponožkobot Skinners](https://www.hithit.com/cs/project/1589/skinners-botky-do-kapsy) si nejdřív ověřili funkčnost konceptu s komunitou, která je jim blízká,“ popisuje jeden z úspěšných nápadů Jana Ecksteinová z Hithitu. „Na našem webu si zajistili přes půl milionu korun na první rozjezd a světu na Kickstarteru představili druhou verzi bot, kterou vylepšili na základě zkušeností a zpětné vazby původních přispěvatelů. Tam získali v přepočtu přes 16 milionů korun, tedy sumu, která je zase pro český trh málo reálná.“

Vůbec nejúspěšnějším českým projektem na Kickstarteru je videohra [Kingdom Come: Deliverance](https://www.kickstarter.com/projects/1294225970/kingdom-come-deliverance) pražského studia Warhorse Studios. Získala v přepočtu téměř 35 milionů korun, což řádově odpovídá souhrnné velikosti všech projektů na českém Hithitu nebo Startovači. Měla by vyjít během roku 2017.

Pro srovnání: největší příspěvek na českých klonech Kickstarteru, 2,8 milionu korun, získala od fanoušků [expedice trabantů do Tichomoří](https://www.startovac.cz/projekty/transtrabant/). Mezi nejúspěšnějšími projekty jsou u nás často ty, které navazují na předchozí úspěšné působení: tichomořská expedice navazuje na několik předchozích výprav v podobném složení řidičů i vozidel; rozhovory v DVTV (2,2 milionu) se hlásí k úspěšnému působení svých redaktorů v České televizi; nápad uspořádat United Islands v Kinského zahradě (1,7 milionu) rozšiřuje úspěšný festival a tak dále. Oba české „nakopávače“ tak často plní spíš roli udržovačů.

<aside class="big">
  <div class="scroll-wrapper kolecka">
    <iframe src="https://samizdat.cz/data/startovace/charts/kolecka.html"></iframe>
  </div>
</aside>

*Údaje jsou v Kč, přepočet dolarů na koruny počítá s kurzem 1:27.*

## Na Startovači spíš uspějete, na Hithitu dostanete víc

Každý z českých webů nabízí svým uživatelům odlišný přístup. Startovač se s autory projektů zaměřuje na úspěšnost, cílovou částku vybere šedesát procent nápadů.

„S autory strávíme mraky času na přípravě kampaní,“ odhaluje Jakub Červinka ze Startovače. „Věnujeme se všem. Navíc umožňujeme založit kampaně od 10 tisíc korun. Nenutíme autory jít zbytečně vysoko, aby si nemuseli sami na projekt něco doplácet.“

Projekty na Hithitu mají oproti Startovači nižší šanci na úspěch – 45 procent – zato získávají větší částku. Zatímco na Startovači získá průměrný projekt 88 tisíc korun, na Hithitu je to 118 tisíc. Průměrná částka se ovšem hodně liší podle oboru, u technologických projektů je na obou webech víc než dvojnásobná oproti těm hudebním.

<aside class="big">
  <iframe src="https://samizdat.cz/data/startovace/charts/castka.html" height="600px" frameborder="0"></iframe>
</aside>

## Kdo rychle získá dvacet procent cílové částky, je za vodou

Jakmile projekty získají požadovanou částku, vzniká jim vůči přispěvatelům právní závazek projekt dokončit. Ne vždy se to ovšem podaří; autoři nápadů často prošlapávají slepé cesty, dávají se dohromady pouze pro konkrétní projekt a hlavně nemusí předem odhadnout zájem o svůj nápad, takže pak produkt nezvládají vyrobit v dostatečném množství. To vše znamená vyšší riziko neúspěchu. Ukázkovým průšvihem je britský startup, který na Kickstarteru získal 3,4 milionu dolarů na miniaturní drony a [do několika měsíců zkrachoval](http://www.mobilmania.cz/bleskovky/vybrali-rekordni-castku-na-kickstarteru-ale-stejne-zkrachovali/sc-4-a-1332653/default.aspx).

„Platí, že kde není žalobce, není soudce,“ doplňuje Petr Tomek ze Startovače. „Když už se ale některý projekt ocitne u soudu, dopad může být tvrdý.“

Čeští tvůrci zatím podobnou kauzu nezažili. Ve Spojených státech ovšem špatně dopadl autor stolní hry The Doom That Came To Atlantic City, který vybral na Kickstarteru 122 tisíc dolarů a [pomohl si jimi k zaplacení nájmu](http://www.toynews-online.biz/news/read/kickstarter-fraudster-to-repay-backers-after-using-funds-for-personal-expenses/044748). Soud na neúspěch zareagoval nařízením vrátit peníze dárcům.

Zda bude projekt úspěšný, se obvykle pozná hned na startu kampaně.

„Kdo vybere dostatečně rychle dvacet procent, je ve většině případů za vodou," vysvětluje Petr Tomek ze Startovače. „Celý crowdfunding je postavený na emocích a kampaně mají psychologii závodu. Nezdařené starty se tedy ukážou poměrně brzy a autoři je taky brzy opouštějí.“

Většina kampaní má proto tvar „kolébky“: většina peněz přichází na začátku kampaně, a pokud projekt bojuje s překonáním cílové částky, pak se příspěvky znovu zvyšují v posledních dnech.

<aside class="big">
  <iframe src="https://samizdat.cz/data/startovace/charts/prubeh.html" height="600px" frameborder="0"></iframe>
</aside>

Někdy ovšem mobilizace komunity nestačí a autorům projektů chvíli před koncem kampaně do cílové částky stále něco chybí. V takovém případě si často peníze na svůj projekt doplatí sami.

„Servery jako Kickstarter takové jednání tvrdě postihují, je to pro ně důvod pro zrušení projektu,“ tvrdí Tomek. „My vycházíme prostě z toho, že autor projektu na projekt peníze potřebuje. Závazek uskutečnit projekt je přitom stejný, i když si na účet nějaké peníze pošle sám.“

„V crowdfundingu to není zakázané,“ doplňuje ho Jana Ecksteinová z Hithitu. „Když se autorovi podaří vybrat omdesát nebo devadesát procent cílové částky, byl by hloupý, kdyby si zbývající částku nedoplatil sám.“

Naprostá většina neúspěšných projektů se proto zadrhne hluboko pod polovinou cílové částky. Většina úspěšných zase cílovou částku překoná jen o několik procent. Superúspěšných projektů je v Česku minimum.

Úspěch kampaně je ostatně také v zájmu Hithitu, Startovače a ostatních crowdfundingových webů. Z každého úspěšně dokončeného projektu si několik procent vezmou.

<aside class="big">
  <iframe src="https://samizdat.cz/data/startovace/charts/uspesnost.html" height="400px" frameborder="0" style="width:800px; display: block; margin-left: auto; margin-right: auto;"></iframe>
</aside>

## Jasně dominuje Praha

Data Hithitu umožňují i srovnání měst, se kterými jsou nápady spojené. Při zadávání projektu na web totiž autoři vyplňují kolonku, kde se bude projekt odehrávat. Na detailní obrázek, které kreativní profese kvetou ve kterém městě, podobný [tomu pro Spojené státy](http://polygraph.cool/kickstarter/), jsou bohužel česká data zatím příliš řídká. Lze pouze srovnat celkovou částku, utracenou za nápady v jednotlivých městech. Z tohoto srovnání vychází naprosto dominantně hlavní město: v Praze uspěly projekty za 28,4 milionu korun, což je téměř desetkrát víc než v druhém Brně s 3,1 milionu. Třetí jsou na Hithitu Košice s úspěšnými nápady za téměř 1,7 milionu korun.

